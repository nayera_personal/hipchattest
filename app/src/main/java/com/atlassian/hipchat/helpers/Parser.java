package com.atlassian.hipchat.helpers;

import android.os.AsyncTask;

import com.atlassian.hipchat.datamodels.ChatMessage;
import com.atlassian.hipchat.datamodels.Link;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nayera on 4/29/2016.
 */
public class Parser {

    ChatMessage chatMessage;
    String inputText;

    public Parser(String _inputText) {
        inputText = _inputText;
        chatMessage = new ChatMessage();
    }

    public String GetJsonMessage() {
        try {
            FindEmoticons(inputText);
            FindMentions(inputText);
            FindLinks(inputText);

            String jsonChatMessage = new Gson().toJson(chatMessage);
            return jsonChatMessage;
        } catch (Exception ex) {
            return null;
        }
    }

    void FindEmoticons(String inputString) {
        try {
            Pattern pattern = Pattern.compile("\\(\\w{1,15}\\)");
            Matcher matcher = pattern.matcher(inputString);
            List<String> emoticonsList = new ArrayList<>();
            while (matcher.find()) {
                String emoticon = matcher.group().replace("(", "").replace(")", "");
                emoticonsList.add(emoticon);
            }
            if (emoticonsList.size() > 0)
                chatMessage.setEmoticons(emoticonsList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void FindMentions(String inputString) {
        try {
            Pattern pattern = Pattern.compile("\\B@[a-z0-9_-]+");
            Matcher matcher = pattern.matcher(inputString);
            List<String> mentionsList = new ArrayList<>();
            while (matcher.find()) {
                String mention = matcher.group().replace("@", "");
                mentionsList.add(mention);
            }
            if (mentionsList.size() > 0)
                chatMessage.setMentions(mentionsList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void FindLinks(String text) {
        try {

            String regex = "\\(?\\b(http://|https://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(text);
            List<Link> linksList = chatMessage.getLinks();
            while (m.find()) {
                String urlStr = m.group();
                if (urlStr.startsWith("(") && urlStr.endsWith(")")) {
                    urlStr = urlStr.substring(1, urlStr.length() - 1);
                }

                if (linksList == null)
                    linksList = new ArrayList<>();


                GetWebPageTitle myTask = new GetWebPageTitle();
                String urlTitle = myTask.execute(urlStr).get();

                Link currentLink = new Link();
                currentLink.setUrl(urlStr);
                currentLink.setTitle(urlTitle);
                linksList.add(currentLink);
            }
            chatMessage.setLinks(linksList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private class GetWebPageTitle extends AsyncTask<Object, Void, String> {

        String url;

        @Override
        protected String doInBackground(Object... params) {

            try {
                url = params[0].toString();

                String pageTitle = WebPageTitleExtractor.GetPageTitle(url);

                return pageTitle;
            } catch (Exception ex) {
                return null;
            }
        }

        @Override

        protected void onPostExecute(String result) {


        }
    }

}
