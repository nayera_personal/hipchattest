package com.atlassian.hipchat;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.atlassian.hipchat.helpers.Parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    EditText txtMsg;
    Button btnSubmit;
    TextView txtOutput;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtMsg = (EditText) findViewById(R.id.txtMsg);
        txtOutput = (TextView) findViewById(R.id.txtOutput);
        txtOutput.setTextIsSelectable(true);

        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(btnSubmitClick);

    }

    View.OnClickListener btnSubmitClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            String inputString = txtMsg.getText().toString();
            if (inputString == null || inputString.equals("")) {
                Toast.makeText(MainActivity.this, "Please enter message", Toast.LENGTH_LONG).show();
            } else {

                Parser parser = new Parser(inputString);
                String jsonStr = parser.GetJsonMessage();
                if (jsonStr != null && !jsonStr.equals("")) {
                    txtOutput.setText(jsonStr);
                } else {
                    txtOutput.setText("");
                    Toast.makeText(MainActivity.this, "Your message could not be parsed", Toast.LENGTH_LONG).show();
                }
            }
        }
    };


}
