package com.atlassian.hipchat.datamodels;

import java.util.List;

/**
 * Created by Nayera on 3/26/2016.
 */
public class ChatMessage {

    private List<String> mentions;
    private List<String> emoticons;
    private List<Link> links;

    public List<String> getEmoticons() {
        return emoticons;
    }

    public void setEmoticons(List<String> emoticons) {
        this.emoticons = emoticons;
    }

    public List<String> getMentions() {
        return mentions;
    }

    public void setMentions(List<String> mentions) {
        this.mentions = mentions;
    }


    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
