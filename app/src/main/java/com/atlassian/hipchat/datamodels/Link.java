package com.atlassian.hipchat.datamodels;

/**
 * Created by Nayera on 4/29/2016.
 */
public class Link {

    private String title;
    private String url;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
